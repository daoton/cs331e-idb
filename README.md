# **Canvas / Ed Discussion group number:**

## Group 13

## **Names of the team members | UTEID:**

- Dao A. Ton-Nu | dt26435
- Emily Zhou | ez3984
- Johnny Henriquez | jah8987
- Alexander Valdez | av35768
- Faiz Shakoor | fas799

## **Name of the project:**

Artology

## **Project URL**

Website Link:

API Base url:

API Documentation:


## **Git SHA**



## **The proposed project:**

Our project acts as a hub of information for exhibition, artworks, and artists in the U.S. We will detail various exhibitions, the artworks and artists featured in them, along with the locations of the exhibitions. The website will raise awareness about mental illnesses, both for people who want to learn more and people who want to be treated.

## **URLs of at least three instances that you will programmatically scrape**

https://api.artic.edu/api/v1/exhibitions
https://api.artic.edu/api/v1/artworks
https://api.artic.edu/api/v1/artists
https://maps.googleapis.com/maps/api/place/findplacefromtext/output?parameters

## **At least three models:**
1) Exhibitions
2) Artworks (featured in exbhitions)
3) Artists (of each artwork)

## **An estimate of the number of instances of each model:**
1) Exhibitions - 12
2) Artworks - 72
3) Artists - 30

## **Each model must have many attributes:**

 
## **Describe attributes for each model that you can filter or sort:**

## **Instances of each model must connect to instances of at least two other models:**



## **Describe two types of media for instances of each model:**



## Describe three questions that your site will answer:**


