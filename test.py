#!/usr/bin/env python3

# ---------------------------
# projects/IDB3/main.py
# Fares Fraij
# ---------------------------

# -------
# imports
# -------

import os
import sys
import unittest
from app import app
from flask_sqlalchemy import SQLAlchemy
from models import app, db, Exhibition, Artist, Artwork
from flask import Flask, render_template, request, url_for, redirect, json, jsonify, Response


app = Flask(__name__) 
app.app_context().push()


# TODO: Configure SQLAlchemy
# Change this accordingly 
USER ="postgres"
PASSWORD ="zhoupsql"
PUBLIC_IP_ADDRESS ="localhost:5432"
DBNAME ="artologydb"

# Configuration 
app.config['SQLALCHEMY_DATABASE_URI'] = \
os.environ.get("DB_STRING",f'postgresql://{USER}:{PASSWORD}@{PUBLIC_IP_ADDRESS}/{DBNAME}')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True  # To suppress a warning message
db = SQLAlchemy(app)


# -----------
# DBTestCases
# -----------
class DBTestCases(unittest.TestCase):
    # --------------------
    # exhibition insertion
    # --------------------

    def test_exhibition_insert_1(self):
        # Check if the record already exists
        existing_record = db.session.query(Exhibition).filter_by(id='1').first()
        if existing_record:
            # If the record already exists, delete it before running the test
            db.session.delete(existing_record)
            db.session.commit()

        # Test insertion into the database using models directly
        s = Exhibition(id='1', title='Test Exhibition')
        db.session.add(s)
        db.session.commit()
    
        r = db.session.query(Exhibition).filter_by(id='1').one()
        self.assertEqual(str(r.id), '1')
        self.assertEqual(str(r.title), 'Test Exhibition')

        # Clean up after the test
        db.session.delete(s)
        db.session.commit()

    # ----------------
    # artist insertion
    # ----------------

    def test_artist_insert_1(self):
        # Check if the record already exists
        existing_record = db.session.query(Artist).filter_by(id='1').first()
        if existing_record:
            # If the record already exists, delete it before running the test
            db.session.delete(existing_record)
            db.session.commit()

        # Test insertion into the database using models directly
        s = Artist(id='1', title='Test Artist')
        db.session.add(s)
        db.session.commit()
    
        r = db.session.query(Artist).filter_by(id='1').one()
        self.assertEqual(str(r.id), '1')
        self.assertEqual(str(r.title), 'Test Artist')

        # Clean up after the test
        db.session.delete(s)
        db.session.commit()

    # -----------------
    # artwork insertion
    # -----------------

    def test_artwork_insert_1(self):
        # Check if the record already exists
        existing_record = db.session.query(Artwork).filter_by(id='1').first()
        if existing_record:
            # If the record already exists, delete it before running the test
            db.session.delete(existing_record)
            db.session.commit()

        # Test insertion into the database using models directly
        s = Artwork(id='1', title='Test Artwork')
        db.session.add(s)
        db.session.commit()
    
        r = db.session.query(Artwork).filter_by(id='1').one()
        self.assertEqual(str(r.id), '1')
        self.assertEqual(str(r.title), 'Test Artwork')

        # Clean up after the test
        db.session.delete(s)
        db.session.commit()


    # --------------------
    # exhibition update
    # --------------------

    def test_exhibition_update_1(self):
        # Insert a test exhibition record into the database
        test_exhibition = Exhibition(id='1', title='Test Exhibition')
        db.session.add(test_exhibition)
        db.session.commit()

        # Update the exhibition record
        updated_title = 'Updated Exhibition Title'
        test_exhibition.title = updated_title
        db.session.commit()

        # Query the database to ensure that the title has been updated
        updated_exhibition = db.session.query(Exhibition).filter_by(id='1').first()
        self.assertEqual(updated_exhibition.title, updated_title)

        # Clean up after the test
        db.session.delete(updated_exhibition)
        db.session.commit()


    # ----------------
    # artist update
    # ----------------

    def test_artist_update_1(self):
        # Insert a test artist record into the database
        test_artist = Artist(id='1', title='Test Artist')
        db.session.add(test_artist)
        db.session.commit()

        # Update the artist record
        updated_title = 'Updated Artist Title'
        test_artist.title = updated_title
        db.session.commit()

        # Query the database to ensure that the title has been updated
        updated_artist = db.session.query(Artist).filter_by(id='1').first()
        self.assertEqual(updated_artist.title, updated_title)

        # Clean up after the test
        db.session.delete(updated_artist)
        db.session.commit()


    # -----------------
    # artwork update
    # -----------------

    def test_artwork_update_1(self):
        # Insert a test artwork record into the database
        test_artwork = Artwork(id='1', title='Test Artwork')
        db.session.add(test_artwork)
        db.session.commit()

        # Update the artwork record
        updated_title = 'Updated Artwork Title'
        test_artwork.title = updated_title
        db.session.commit()

        # Query the database to ensure that the title has been updated
        updated_artwork = db.session.query(Artwork).filter_by(id='1').first()
        self.assertEqual(updated_artwork.title, updated_title)

        # Clean up after the test
        db.session.delete(updated_artwork)
        db.session.commit()


    # --------------------
    # exhibition deletion
    # --------------------

    def test_exhibition_delete_1(self):
        # Insert a test exhibition record into the database
        test_exhibition = Exhibition(id='1', title='Test Exhibition')
        db.session.add(test_exhibition)
        db.session.commit()

        # Delete the exhibition record
        db.session.delete(test_exhibition)
        db.session.commit()

        # Query the database to ensure that the record has been deleted
        deleted_exhibition = db.session.query(Exhibition).filter_by(id='1').first()
        self.assertIsNone(deleted_exhibition)

    # ----------------
    # artist deletion
    # ----------------

    def test_artist_delete_1(self):
        # Insert a test artist record into the database
        test_artist = Artist(id='1', title='Test Artist')
        db.session.add(test_artist)
        db.session.commit()

        # Delete the artist record
        db.session.delete(test_artist)
        db.session.commit()

        # Query the database to ensure that the record has been deleted
        deleted_artist = db.session.query(Artist).filter_by(id='1').first()
        self.assertIsNone(deleted_artist)

    # -----------------
    # artwork deletion
    # -----------------

    def test_artwork_delete_1(self):
        # Insert a test artwork record into the database
        test_artwork = Artwork(id='1', title='Test Artwork')
        db.session.add(test_artwork)
        db.session.commit()

        # Delete the artwork record
        db.session.delete(test_artwork)
        db.session.commit()

        # Query the database to ensure that the record has been deleted
        deleted_artwork = db.session.query(Artwork).filter_by(id='1').first()
        self.assertIsNone(deleted_artwork)


    #-----------------------
    # artist-artwork relationship
    # -----------------------
    def test_artist_artwork_relationship(self):
        # Create a new artist
        artist = Artist(id='1', title='Test Artist')
        db.session.add(artist)
        db.session.commit()

        # Create some artworks and associate them with the artist
        artwork1 = Artwork(id='1', title='Artwork 1', artist_id='1')
        artwork2 = Artwork(id='2', title='Artwork 2', artist_id='1')
        db.session.add(artwork1)
        db.session.add(artwork2)
        db.session.commit()

        # Query the artist and verify their artworks
        queried_artist = db.session.query(Artist).filter_by(id='1').one()
        self.assertEqual(len(queried_artist.artworks), 2)
        self.assertEqual(queried_artist.artworks[0].title, 'Artwork 1')
        self.assertEqual(queried_artist.artworks[1].title, 'Artwork 2')

        # Clean up after the test
        db.session.delete(artwork1)
        db.session.delete(artwork2)
        db.session.delete(artist)
        db.session.commit()


if __name__ == '__main__':
    unittest.main()
# end of code
