from google.cloud import storage
import pandas as pd
import requests
import requests_cache
import json
import os
from datetime import datetime, timezone



def write_json(data, file_path):
    with open(file_path, 'w', encoding='utf-8') as f:
        json.dump(data, f, ensure_ascii=False, indent=4)

# Access the environment variable
service_account_path = os.getenv('GOOGLE_APPLICATION_CREDENTIALS')

# Initialize lists to hold all data
all_exhibitions = []
all_artists = []
all_artworks = []
fields_string = {
    'exhibitions':  'id,api_model,api_link,title,is_featured,short_description,web_url,image_url,status,aic_start_at,aic_end_at,gallery_id,gallery_title,artwork_ids,artwork_titles,artist_ids,image_id,alt_image_ids,source_updated_at',

    'artworks':     'id,api_model,api_link,title,alt_titles,thumbnail,boost_rank,date_start,date_end,date_display,date_qualifier_title,date_qualifier_id,artist_display,place_of_origin,dimensions,medium_display,publication_history,exhibition_history,edition,is_zoomable,max_zoom_window_size,has_multimedia_resources,color,is_on_view,artwork_type_title,artwork_type_id,department_title,department_id,artist_id,artist_title,category_ids,category_titles,style_id,style_title,subject_id,subject_titles,material_id,material_titles,technique_ids,technique_titles,image_id,video_ids,site_ids,source_updated_at',

    'artists':       'id,api_model,api_link,title,sort_title,alt_titles,birth_date,death_date,description,ulan_id,source_updated_at&is_artist=true',

    'artwork-types':'id,api_model,api_link,title,aat_id,source_updated_at'

}

def write_json(data, file_path):
    """Writes data to a JSON file."""
    with open(file_path, 'w', encoding='utf-8') as f:
        json.dump(data, f, ensure_ascii=False, indent=4)


def fetch_artwork_and_artist_info(artwork_id, fields_string):
    """Fetches artwork information and the corresponding artist."""
    artwork_url = f"https://api.artic.edu/api/v1/artworks/{artwork_id}"#&fields={fields_string['artworks']}"
    artwork_response = requests.get(artwork_url)
    if artwork_response.status_code == 200:
        artwork_data = artwork_response.json().get('data', {})
        all_artworks.append(artwork_data)  # Append artwork data
        
        artist_id = artwork_data.get('artist_id')
        if artist_id and artist_id not in [artist['id'] for artist in all_artists]:  # Check for duplicates
            fetch_artist_info(artist_id, fields_string)
    else:
        print(f"Error fetching artwork {artwork_id}: {artwork_response.status_code}")


def fetch_artist_info(artist_id, fields_string):
    """Fetches and processes artist information."""
    artist_url = f"https://api.artic.edu/api/v1/agents/{artist_id}"#&fields={fields_string['artists']}"
    artist_response = requests.get(artist_url)
    if artist_response.status_code == 200:
        artist_data = artist_response.json().get('data', {})
        all_artists.append(artist_data)  # Append artist data
        print(f"Processed artist {artist_id}")
    else:
        print(f"Error fetching artist {artist_id}: {artist_response.status_code}")


def fetch_and_process_exhibitions(fields_string):
    """Fetches exhibitions and filters them based on open/closed status."""
    exhibitions_url = f"https://api.artic.edu/api/v1/exhibitions?limit=100&page=5"#&fields={fields_string['exhibitions']}"  # Adjust as needed
    response = requests.get(exhibitions_url)
    if response.status_code == 200:
        exhibitions_data = response.json().get('data', [])
        current_datetime = datetime.now(timezone.utc)  # Ensure current_datetime is timezone-aware

        open_exhibitions = []
        closed_exhibitions = []

        for ex in exhibitions_data:
            aic_start_at = ex.get('aic_start_at')
            aic_end_at = ex.get('aic_end_at')
            artwork_ids = ex.get('artwork_ids', [])
            artist_ids = ex.get('artist_ids', [])
            
            # Skip exhibitions without artwork_ids or artist_ids
            if not artwork_ids or not artist_ids:
                continue

            # Convert start and end dates to datetime objects, handling None for aic_end_at
            if aic_start_at:
                aic_start_datetime = datetime.fromisoformat(aic_start_at[:19]).replace(tzinfo=timezone.utc)
                is_open = not aic_end_at or datetime.fromisoformat(aic_end_at[:19]).replace(tzinfo=timezone.utc) >= current_datetime
                
                # Determine if the exhibition is currently open or has closed
                if is_open:
                    open_exhibitions.append(ex)
                else:
                    closed_exhibitions.append(ex)

        # Prioritize open exhibitions, but include closed ones if fewer than 2 open exhibitions are available
        selected_exhibitions = open_exhibitions[:2] + closed_exhibitions[:max(0, 10-len(open_exhibitions))]

        for exhibition in selected_exhibitions:
            all_exhibitions.append(exhibition)  # Append exhibition data
            
            artwork_ids = exhibition.get('artwork_ids', [])
            for artwork_id in artwork_ids:
                fetch_artwork_and_artist_info(artwork_id, fields_string)

        # After collecting all data, write to respective JSON files
        write_json(all_exhibitions, './data/exhibitions.json')
        write_json(all_artists, './data/artists.json')
        write_json(all_artworks, './data/artworks.json')
        exhibitions_data = response.json().get('data', [])
        current_datetime = datetime.now(timezone.utc)  # Ensure current_datetime is timezone-aware

        open_exhibitions = []
        closed_exhibitions = []

        for ex in exhibitions_data:
            aic_start_at = ex.get('aic_start_at')
            aic_end_at = ex.get('aic_end_at')
            artwork_ids = ex.get('artwork_ids', [])
            artist_ids = ex.get('artist_ids', [])
            
            # Skip exhibitions without artwork_ids or artist_ids
            if not artwork_ids or not artist_ids:
                continue

            # Convert start and end dates to datetime objects, handling None for aic_end_at
            if aic_start_at:
                aic_start_datetime = datetime.fromisoformat(aic_start_at[:19]).replace(tzinfo=timezone.utc)
                is_open = not aic_end_at or datetime.fromisoformat(aic_end_at[:19]).replace(tzinfo=timezone.utc) >= current_datetime
                
                # Determine if the exhibition is currently open or has closed
                if is_open:
                    open_exhibitions.append(ex)
                else:
                    closed_exhibitions.append(ex)

        # Prioritize open exhibitions, but include closed ones if fewer than 2 open exhibitions are available
        selected_exhibitions = open_exhibitions[:2] + closed_exhibitions[:max(0, 10-len(open_exhibitions))]

        for exhibition in selected_exhibitions:
            all_exhibitions.append(exhibition)  # Append exhibition data
            
            artwork_ids = exhibition.get('artwork_ids', [])
            for artwork_id in artwork_ids:
                fetch_artwork_and_artist_info(artwork_id, fields_string)

        # After collecting all data, write to respective JSON files
        write_json(all_exhibitions, './data/exhibitions.json')
        write_json(all_artists, './data/artists.json')
        write_json(all_artworks, './data/artworks.json')
    else:
        print(f"Error fetching exhibitions: {response.status_code}")

fetch_and_process_exhibitions(fields_string)

# Scraping Images: https://api.artic.edu/docs/#iiif-image-api
#TODO: Artist needs > 1 artwork