from flask import Flask, render_template, request, url_for, redirect, json, jsonify, Response
from math import ceil
from datetime import datetime
from dateutil import parser
from collections import defaultdict
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import or_, cast, String, func
from create_db import app, db, Exhibition, Artist, Artwork
import os
import re

def sort_by_date(exhibition):
    end_date = exhibition.get('end_date', '')
    if end_date.lower() == 'current':
        return datetime.max
    try:
        return datetime.strptime(end_date, '%d-%m-%Y')
    except ValueError:
        # If there's an issue with the date format, return a date far in the past
        return datetime.min


def format_date(value, format='%b %d, %Y'):
    """Convert a date string to a specified format."""
    
    if value:
        if isinstance(value, str) and value.lower() == 'current':
            return 'Current'

        try:
            date = parser.parse(value)
            return date.strftime(format)
        except ValueError:
            date = datetime.strptime(value, '%d-%m-%Y')
            return date.strftime(format)
    return ''


model_map = {
    'ARTISTS': Artist,
    'ARTWORKS': Artwork,
    'EXHIBITIONS': Exhibition,
}

app.jinja_env.filters['format_date'] = format_date


def perform_search_and_sort(model, search_variable=None, search_query=None, sort_by=None, sort_order='asc'):
    query = model.query

    if search_variable and search_query:
        model_attr = getattr(model, search_variable)
        query = query.filter(model_attr.contains(search_query))

    # Adjust sort_by for special cases
    if sort_by == 'recent':
        sort_by = 'aic_start_at'
    if sort_by == 'creation_date':
        sort_by = 'date_start'

    if sort_by and hasattr(model, sort_by):
        sort_attr = getattr(model, sort_by)
        if sort_order == 'desc':
            query = query.order_by(sort_attr.desc())
        else:
            query = query.order_by(sort_attr)

    return query.all()

@app.route('/api/<model>/')
@app.route('/api/search/<model>/')
@app.route('/api/get_all_<model>/')
def get_all(model):

    model_name = model.upper()
    model = model_map.get(model_name)
    search_variable = request.args.get('search_variable')
    search_query = request.args.get('search_query')
    sort_by = request.args.get('sort_by', 'title')  # Default sort by 'id' or any default field
    sort_order = request.args.get('sort_order', 'asc')

    if not model:
        return jsonify({'error': 'Invalid model name'}), 404    

    results = perform_search_and_sort(model, search_variable, search_query, sort_by, sort_order)

    serialized_items = [item.serialize() for item in results]

    return jsonify(serialized_items)

# GET model by ID
@app.route('/api/<model_title>/<int:model_id>/')
def searchModelID(model_title, model_id):
    model_name = model_title.upper()
    model = model_map.get(model_name)

    if not model:
        return jsonify({'error': 'Invalid model name'}), 404

    found_model = db.session.query(model).filter(model.id == model_id).first()
    if found_model:
        # If the artwork is found, serialize the data and return as JSON
        found_model_data = found_model.serialize()
        return jsonify(found_model_data)
    else:
        # If artwork is not found, return a JSON response with an error message
        return jsonify({'error': 'Not found'}), 404


def addRelationShips(model_name, model_id, model_dict):

    model_name = model_name.upper()
    model = model_map.get(model_name)

    if not model:
        return jsonify({'error': 'Invalid model name'}), 404
    
    found_model = db.session.query(model).filter(model.id == model_id).first()

    if (model_name == 'ARTISTS'):
        model_dict['artworks']= found_model.artworks
        model_dict['exhibitions']= found_model.exhibitions
    elif (model_name == 'EXHIBITIONS'):
        model_dict['artworks']= found_model.artworks
        model_dict['artists']= found_model.artists
    else:
        model_dict['exhibitions'] = found_model.exhibitions

    return model_dict


@app.route('/', methods=['GET','POST'])
def index():
	return render_template('index.html', active_page='home')

@app.route('/about/')
def about():
     return render_template('about.html', active_page='about')


@app.route('/artworks/')
def showArtworks():
    search = request.args.get('query', '')
    page = request.args.get('page', 1, type=int)
    sort_by = request.args.get('sort_by', 'title')
    sort_order = request.args.get('order', 'asc')
    per_page = 18
    query = Artwork.query
    if search:
        query = query.filter(
            or_(
                Artwork.title.ilike(f'%{search}%'),
                Artwork.medium_display.ilike(f'%{search}%'),
                Artwork.artwork_type_title.ilike(f'%{search}%'),
                Artwork.artist_title.ilike(f'%{search}%')
            )
        )
    
    if hasattr(Artwork, sort_by):
        sort_attr = getattr(Artwork, sort_by)
        query = query.order_by(sort_attr.desc()) if sort_order == 'desc' else query.order_by(sort_attr)

    paginated_artworks = query.paginate(page=page, per_page=per_page, error_out=False)
    return render_template('artworks.html', artworks = paginated_artworks.items, query=search, sort_by=sort_by, order=request.args.get('order', 'ascending'), page=page, current_page=page, total_pages=paginated_artworks.pages, func_name = 'showArtworks', totNum = paginated_artworks.total, active_page='artworks')

@app.route('/artworks/<int:artwork_id>/')
def oneArtwork(artwork_id):

    try:
        artwork_dict = (searchModelID("artworks",artwork_id)).get_json()
        artwork_dict = addRelationShips("artworks", artwork_id, artwork_dict)
        return render_template('artwork-info.html', artwork = artwork_dict, active_page='artworks')
    except:
        return redirect(url_for("showArtworks")) 



@app.route('/Artists/')
@app.route('/artists/')
def showArtists():
    search = request.args.get('query', '')
    page = request.args.get('page', 1, type=int)
    sort_by = request.args.get('sort_by', 'title')
    sort_order = request.args.get('order', 'asc')
    per_page = 12

    query = Artist.query
    if search:
        query = query.filter(
            or_(
                Artist.title.ilike(f'%{search}%'),
                Artist.description.ilike(f'%{search}%'),
                cast(Artist.birth_date, String).ilike(f'%{search}%'),
                cast(Artist.death_date, String).ilike(f'%{search}%')    
                )
        )

    if hasattr(Artist, sort_by):
        sort_attr = getattr(Artist, sort_by)
        query = query.order_by(sort_attr.desc()) if sort_order == 'desc' else query.order_by(sort_attr)

    paginated_artists = query.paginate(page=page, per_page=per_page, error_out=False)
    return render_template('artists.html', artists = paginated_artists.items, query=search, sort_by=sort_by, order=request.args.get('order', 'ascending'), page=page, total_pages=paginated_artists.pages, func_name = 'showArtists', totNum = paginated_artists.total, active_page='artists')


@app.route('/artists/<int:artist_id>/')
def oneArtist(artist_id):
    try:
        artist_dict = (searchModelID("artists",artist_id)).get_json()
        artist_dict = addRelationShips("artists", artist_id, artist_dict)
        return render_template('artist-info.html', artist = artist_dict, active_page='artists')
    except:
        return redirect(url_for("showArtists")) 



@app.route('/exhibitions/')
def showExhibitions():
    now = datetime.now()
    search = request.args.get('query', '')
    page = request.args.get('page', 1, type=int)
    sort_by = request.args.get('sort_by', 'aic_start_at')  # Assuming 'aic_start_at' for 'recent'
    sort_order = 'desc' if request.args.get('order', 'ascending') == 'descending' else 'asc'
    per_page = 6
    
    query = Exhibition.query
    if search:
        query = query.filter(
            or_(
                Exhibition.title.ilike(f'%{search}%'),
                Exhibition.short_description.ilike(f'%{search}%')
            )
        )
    
    if hasattr(Exhibition, sort_by):
        sort_attr = getattr(Exhibition, sort_by)
        query = query.order_by(sort_attr.desc()) if sort_order == 'desc' else query.order_by(sort_attr)

    exhibitions = query.paginate(page=page, per_page=per_page, error_out=False)

    return render_template('exhibitions.html', exhibitions=exhibitions.items, query=search, func_name='showExhibitions', page=page, total_pages=exhibitions.pages, sort_by=sort_by, order=request.args.get('order', 'ascending'), now=now, active_page='exhibitions')

@app.route('/exhibitions/<int:exhibition_id>/')
def oneExhibition(exhibition_id):
    now = datetime.now()
    exhib_dict = (searchModelID("exhibitions",exhibition_id)).get_json()
    exhib_dict = addRelationShips("exhibitions", exhibition_id, exhib_dict)
    if exhib_dict['aic_start_at']:
        exhib_dict['aic_start_at'] = datetime.strptime(format_date(exhib_dict['aic_start_at']),'%b %d, %Y')
    if exhib_dict['aic_end_at']:    
        exhib_dict['aic_end_at'] = datetime.strptime(format_date(exhib_dict['aic_end_at']),'%b %d, %Y')

    artworks = exhib_dict.get('artworks', [])

    return render_template('exhibition-info.html', exhibition=exhib_dict, now=now,artworks=artworks,active_page='exhibitions')

 

@app.template_filter('format_date')
def format_date(value, format='%b %d, %Y'):
    if value.lower() == 'current':
        return 'Current'
    try:
        date = datetime.strptime(value, '%d-%m-%Y')  # Assuming the date format is day-month-year
        return date.strftime(format)
    except ValueError:
        try:
            date = datetime.strptime(value[:16], '%a, %d %b %Y')
            return date.strftime(format)
        except:
            return value


def create_highlighted_snippet(text, search_query, words_before=5, words_after=5):
    # Compile a regular expression pattern for all search terms, case-insensitive
    terms_pattern = '|'.join(re.escape(term) for term in search_query.split())
    pattern = re.compile(terms_pattern, re.IGNORECASE)

    # Find all matches
    matches = list(pattern.finditer(text))
    if not matches:
        return text[:100] + '...' if len(text) > 100 else text

    # Use the first match to determine the snippet range
    first_match = matches[0]
    start_pos = first_match.start()

    # Find word boundaries to accurately calculate start and end positions
    word_boundaries = [m.start() for m in re.finditer(r'\b', text)]
    # Find the closest word boundary before the match
    snippet_start = next((b for b in reversed(word_boundaries) if b <= start_pos), 0)
    # Extend the search for word boundaries beyond the initial words_after to avoid index errors
    snippet_end_candidates = word_boundaries[snippet_start // 2:]
    snippet_end_index = min(len(snippet_end_candidates) - 1, words_before + words_after)
    snippet_end = snippet_end_candidates[snippet_end_index] if snippet_end_candidates else len(text)

    # Extract and highlight the snippet
    snippet = text[snippet_start:snippet_end]
    highlighted_snippet = pattern.sub(lambda m: f'<mark class="highlight" style="padding:0";>{m.group()}</mark>', snippet)

    # Add leading and trailing ellipses if not at the start/end of the text
    if snippet_start > 0:
        highlighted_snippet = '... ' + highlighted_snippet
    if snippet_end < len(text):
        highlighted_snippet += ' ...'

    return highlighted_snippet


app.template_filter('highlight')(create_highlighted_snippet)


@app.route('/search/')
def search():
    query = request.args.get('query', '')
    results = defaultdict(list)
    now = datetime.utcnow()
    if query:
        exhibitions = Exhibition.query.filter(
            or_(
                Exhibition.title.ilike(f'%{query}%'),
                Exhibition.short_description.ilike(f'%{query}%')
                )).all()

        if exhibitions: results['Exhibition'].extend(exhibitions)
        
        artists = Artist.query.filter(
            or_(
                Artist.title.ilike(f'%{query}%'),
                Artist.description.ilike(f'%{query}%'),
                cast(Artist.birth_date, String).ilike(f'%{query}%'),
                cast(Artist.death_date, String).ilike(f'%{query}%'),
                )
        ).all()
        
        if artists: results['Artist'].extend(artists)

        artworks = Artwork.query.filter(
            or_(
                Artwork.title.ilike(f'%{query}%'),
                Artwork.medium_display.ilike(f'%{query}%'),
                Artwork.artwork_type_title.ilike(f'%{query}%')
                )).all()
        if artworks: results['Artwork'].extend(artworks)

    return render_template('search-results.html', results=results, totNum=len(exhibitions)+len(artists)+len(artworks), now=now, search_query=query)


if __name__ == '__main__':
    app.run(debug=True)
