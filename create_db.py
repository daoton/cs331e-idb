#!/usr/bin/env python3
from google.cloud import storage
from models import app, db, Exhibition, Artist, Artwork
from sqlalchemy.exc import IntegrityError
from datetime import datetime
from dateutil import parser
import requests
import json
# import logging
# psql -U postgres -h localhost

# Configure logging
# logging.basicConfig(filename='app.log', level=logging.INFO)

# Log some messages
# logging.debug('This is a debug message')
# logging.info('This is an info message')
# logging.warning('This is a warning message')
# logging.error('This is an error message')
# logging.critical('This is a critical message')
# ------------
# load_json
# ------------
def load_json(filename):
    """
    return a python dict jsn
    filename a json file
    """
    with open(filename, encoding="utf-8") as file:
        jsn = json.load(file)
        file.close()

    return jsn


def parse_iso_datetime(input_str):
    if not isinstance(input_str, str):
        return None  # or handle the error as appropriate for your application

    try:
        # print(input_str)
        return datetime.strptime(input_str[:10],  "%Y-%m-%d")#fromisoformat(input_str)
    except ValueError:
        return None

# ------------
# create_exhibitions
# ------------
def get_exhibition(exhibition_id):
    find_exhibition = db.session.query(Exhibition).filter_by(id=exhibition_id).first()
    return find_exhibition

def create_exhibitions():
    """
    populate exhibition table
    """
    exhibitions = load_json('./data/exhibitions.json')
    # DEBUG - TODO remove
    numArtworksInExhibs = 0
    numArtistsInExhibs = 0

    # Assuming 'exhibitions' is a dictionary containing exhibition data under the 'data' key
    for exhibition in exhibitions:
        # logging.debug('exhibition id test', exhibition['id'])
        if get_exhibition(exhibition['id']) is None: # if exhibition DNE in db, add it
            id = exhibition['id']
            api_model = exhibition['api_model']
            api_link = exhibition['api_link']
            title = exhibition['title']
            # print(title)
            is_featured = exhibition['is_featured']
            short_description = exhibition['short_description']
            web_url = exhibition['web_url']
            image_url = exhibition['image_url']
            status = exhibition['status']
            aic_start_at = parse_iso_datetime(exhibition['aic_start_at'])
            # print(aic_start_at)
            aic_end_at = parse_iso_datetime(exhibition['aic_end_at'])
            # print(aic_end_at)
            gallery_id = exhibition['gallery_id']
            gallery_title = exhibition['gallery_title']

            test_artwork_ids = exhibition.get('artwork_ids', [])  # Assuming it's a list of integers
            artwork_ids = []
            artworks = []
            
            for artwork_id in test_artwork_ids:
                exists_artwork = get_artwork(artwork_id)
                if exists_artwork is None:
                    actual_artwork_id = None
                else:
                    # DEBUG TODO remove
                    numArtworksInExhibs = numArtworksInExhibs+1
                    # print("Number of Artwork/Exhib relations found:" + str(numArtworksInExhibs))
                    
                    actual_artwork_id = artwork_id
                    artworks.append(exists_artwork)
                artwork_ids.append(actual_artwork_id)

            artwork_titles = exhibition.get('artwork_titles', [])  # Assuming it's a list of strings 
            # IMPORTANT - titles and ids may mismatch? may need to check

            test_artist_ids = exhibition.get('artist_ids', [])  # Assuming it's a list of integers
            artist_ids = []
            artists = []

            for artist_id in test_artist_ids:
                exists_artist = get_artist(artist_id)
                if exists_artist is None:
                    actual_artist_id = None
                else:
                    # DEBUG TODO remove
                    numArtistsInExhibs = numArtistsInExhibs+1
                    # print("Number of ArtIST/Exhib relations found:" + str(numArtistsInExhibs))
                    
                    actual_artist_id = artwork_id
                    artists.append(exists_artist)
                artist_ids.append(actual_artist_id)

            image_id = exhibition['image_id']
            alt_image_ids = exhibition.get('alt_image_ids', [])  # Assuming it's a list of UUID strings
            source_updated_at = exhibition['source_updated_at']  # Make sure to convert to datetime if necessary
        
            
            # Creating a new Exhibition instance with the extracted values
            newExhibition = Exhibition(
                id=id,
                api_model=api_model,
                api_link=api_link,
                title=title,
                is_featured=is_featured,
                short_description=short_description,
                web_url=web_url,
                image_url=image_url,
                status=status,
                aic_start_at=aic_start_at,
                aic_end_at=aic_end_at,
                gallery_id=gallery_id,
                gallery_title=gallery_title,
                artwork_ids=artwork_ids,
                artworks = artworks,
                artwork_titles=artwork_titles,
                artist_ids=artist_ids,
                artists = artists,
                image_id=image_id,
                alt_image_ids=alt_image_ids,
                source_updated_at=source_updated_at
            )

            # After I create the book, I can then add it to my session. 
            db.session.add(newExhibition)
            # commit the session to my DB.
            db.session.commit()

    # DEBUG TODO remove
    # logging.debug("Total ArtWORK/Exhib relations found:" + str(numArtworksInExhibs))
    # logging.debug("Total ArtIST/Exhib relations found:" + str(numArtistsInExhibs))

        
# ------------
# create_artists
# ------------
def get_artist(artist_id):
    find_artist = db.session.query(Artist).filter_by(id=artist_id).first()
    return find_artist 

def create_artists():
    """
    populate artist table
    """
    artists = load_json('./data/artists.json')

    for artist in artists:
        if get_artist(artist['id']) is None:
            id = artist['id']
            api_model = artist['api_model']
            api_link = artist['api_link']
            title = artist['title']
            sort_title = artist['sort_title']
            alt_titles = artist['alt_titles']
            birth_date = artist['birth_date']
            death_date = artist['death_date']
            description = artist['description']
            ulan_id = artist['ulan_id']
            source_updated_at = artist['source_updated_at'] 
            # artworks = Artist.query.get(id).artworks
            # exhibs
            # TODO: ensure added to new artist
            
            # Creating a new Artist instance with the extracted values
            newArtist = Artist(
                id=id,
                api_model=api_model,
                api_link=api_link,
                title=title,
                sort_title = sort_title,
                alt_titles = alt_titles,
                birth_date = birth_date,
                death_date = death_date,
                description = description,
                ulan_id = ulan_id,
                source_updated_at=source_updated_at,

                # artworks = artworks,
                # exhibitions = exhibitions
            )


            db.session.add(newArtist)
            db.session.commit()

# ------------
# create_artworks
# ------------
def get_artwork(artwork_id):
    find_artwork = db.session.query(Artwork).filter_by(id=artwork_id).first()
    return find_artwork  


def create_artworks():
    """
    populate artwork table
    """
    artworks = load_json('./data/artworks.json')
    # DEBUG - TODO remove
    # numNotFoundArts = 0
    # numFoundArts = 0

    # Assuming 'artworks' is a dictionary containing artwork data under the 'data' key
    for artwork in artworks:
        if get_artwork(artwork['id']) is None:
            id = artwork['id']
            api_model = artwork['api_model']
            api_link = artwork['api_link']
            title = artwork['title']
            alt_titles = artwork['alt_titles']
            date_start = artwork['date_start']
            date_end = artwork['date_end']
            date_display = artwork['date_display']
            artist_display = artwork['artist_display']
            place_of_origin = artwork['place_of_origin']
            medium_display = artwork['medium_display']
            exhibition_history = artwork['exhibition_history']
            is_on_view = artwork['is_on_view']
            artwork_type_title = artwork['artwork_type_title']
            department_title = artwork['department_title']

            # artist_id = artwork['artist_id'] # from models.py: nullable = True
            if get_artist(artwork['artist_id']) is None:
                artist_id = None

                # DEBUG - TODO remove
                # numNotFoundArts = numNotFoundArts + 1
            else:
                artist_id = artwork['artist_id']

                # DEBUG - TODO remove
                # numFoundArts = numFoundArts + 1
                # print(str(numNotFoundArts) + ' artists not in DB. We have found ids for ' + str(numFoundArts))


            # check that we have that artist, otherwise null it
            # get_artist()

            artist_title = artwork['artist_title']
            style_title = artwork['style_title']
            image_id = artwork['image_id']  # Assuming UUID is stored as string
            source_updated_at = artwork['source_updated_at']

            # TODO and ensure added to new artwork
            # exhibitions = db.relationship('Exhibition', secondary=exhibitions_artworks_table, back_populates='artworks') #lazy = true        
            
            # Creating a new Artwork instance with the extracted values
            newArtwork = Artwork(
                id = id,
                api_model = api_model,
                api_link = api_link,
                title = title,
                alt_titles = alt_titles,
                date_start = date_start,
                date_end = date_end,
                date_display = date_display,
                artist_display = artist_display,
                place_of_origin = place_of_origin,
                medium_display = medium_display,
                exhibition_history = exhibition_history,
                is_on_view = is_on_view,
                artwork_type_title = artwork_type_title,
                department_title = department_title,

                artist_id = artist_id, # nullable = True
                # artist = artist # TODO adjust as needed

                artist_title = artist_title,
                style_title = style_title,
                image_id = image_id,  # Assuming UUID is stored as string
                source_updated_at = source_updated_at,
                # TODO and ensure added to new artwork
                # exhibitions = exhibitions      
                exhibition_ids = []
            )
            try:
                db.session.add(newArtwork)
                db.session.commit()
            except IntegrityError as e:
                db.session.rollback()
                print(f"Skipping duplicate record with id {newArtwork.id}.")
            # db.session.add(newArtwork)
            # # commit the session to my DB.
            # db.session.commit()

        
with app.app_context(): 
    db.drop_all()
    db.create_all()

    try:
        # Note: artists' relationships are created with the other two creation functions, since artist itself does not have fields 
        # linking to all of their related artpieces / exhibitions.
        create_artists()

        create_artworks() # artwork.artist_id and artist.artworks
        # also link the rest back: artist.exhibitions, artwork.exhibitions

        create_exhibitions() 

        # Commit the changes to the database
        db.session.commit()
        # print('Sucess.')
    
    except Exception as e:
        print(e)

