#################################################
##### Here we define the schema of each entitity
#################################################

from flask import Flask, render_template, request, url_for, redirect, json, jsonify, Response
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import DDL, event
import os

'''
To authenticate this application with Google Cloud services:
Before running this Flask application, in cmd
set the GOOGLE_APPLICATION_CREDENTIALS environment variable to the path of your Google Cloud service account key file (get this file from gcp):
    
    Mac: export GOOGLE_APPLICATION_CREDENTIALS="/path/to/your/service-account-file.json"
    Windows: set GOOGLE_APPLICATION_CREDENTIALS=C:\path\to\your\service-account-file.json

To run this application: 
    
    flask run

Visit http://127.0.0.1:5000/ in your web browser, and you should see a list of files from your GCS bucket serialized as JSON. 
'''

app = Flask(__name__) 
app.app_context().push()


# Change this accordingly 
USER ="postgres"
PASSWORD ="zhoupsql"
PUBLIC_IP_ADDRESS ="localhost:5432"
DBNAME ="artologydb"


# Make these command line arguments that provide when you deploy the app
# or use other options like connecting directly from App Engine


# Configuration 
app.config['SQLALCHEMY_DATABASE_URI'] = \
os.environ.get("DB_STRING",f'postgresql://{USER}:{PASSWORD}@{PUBLIC_IP_ADDRESS}/{DBNAME}')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True  # To suppress a warning message
db = SQLAlchemy(app)


exhibitions_artists_table = db.Table('exhibitions_artists',
    db.Column('artist_id', db.Integer, db.ForeignKey('artist.id')),
    db.Column('exhibition_id', db.Integer, db.ForeignKey('exhibition.id'))
)

exhibitions_artworks_table = db.Table('exhibitions_artworks',
    db.Column('artwork_id', db.Integer, db.ForeignKey('artwork.id')),
    db.Column('exhibition_id', db.Integer, db.ForeignKey('exhibition.id'))
)


class Exhibition(db.Model):

    __tablename__ = 'exhibition'
	
    id = db.Column(db.Integer, primary_key=True)
    api_model = db.Column(db.String(255))
    api_link = db.Column(db.String(255))
    title = db.Column(db.String(255))
    is_featured = db.Column(db.Boolean)
    short_description = db.Column(db.Text)
    web_url = db.Column(db.String(255))
    image_url = db.Column(db.String(255))
    status = db.Column(db.String(20))
    aic_start_at = db.Column(db.DateTime(timezone=None))
    aic_end_at = db.Column(db.DateTime(timezone=None))
    gallery_id = db.Column(db.Integer)
    gallery_title = db.Column(db.String(255))

    artwork_ids = db.Column(db.ARRAY(db.Integer))
    artworks = db.relationship('Artwork', secondary=exhibitions_artworks_table, back_populates = 'exhibitions')

    artwork_titles = db.Column(db.ARRAY(db.Text))

    artist_ids = db.Column(db.ARRAY(db.Integer))
    artists = db.relationship('Artist', secondary=exhibitions_artists_table, back_populates = 'exhibitions')

    image_id = db.Column(db.String(36))  # Assuming UUID is stored as string
    alt_image_ids = db.Column(db.ARRAY(db.String(36)))  # Assuming UUID is stored as string
    source_updated_at = db.Column(db.DateTime)

    def searchable_text(self):
        return f"{self.title} {self.short_description} {self.artwork_titles} "

    # ------------
    # serialize
    # ------------
    def serialize(self):
        """
        returns a dictionary
        """
        return {
            'id': self.id,
            'api_model': self.api_model,
            'api_link': self.api_link,
            'title': self.title,
            'is_featured': self.is_featured,
            'short_description': self.short_description,
            'web_url': self.web_url,
            'image_url': self.image_url,
            'status': self.status,
            'aic_start_at': self.aic_start_at if self.aic_start_at else None,
            'aic_end_at': self.aic_end_at if self.aic_end_at else None,
            'gallery_id': self.gallery_id,
            'gallery_title': self.gallery_title,
            'artwork_ids': self.artwork_ids,
            'artwork_titles': self.artwork_titles,
            'artist_ids': self.artist_ids,
            'image_id': self.image_id,
            'alt_image_ids': self.alt_image_ids,
            'source_updated_at': self.source_updated_at.isoformat() if self.source_updated_at else None,
        }


class Artwork(db.Model):

    __tablename__ = 'artwork'

    id = db.Column(db.Integer, primary_key=True)
    api_model = db.Column(db.String(255))
    api_link = db.Column(db.String(255))
    title = db.Column(db.Text)
    alt_titles = db.Column(db.ARRAY(db.String(255)))
    date_start = db.Column(db.Integer)
    date_end = db.Column(db.Integer)
    date_display = db.Column(db.String(255))
    artist_display = db.Column(db.String(255))
    place_of_origin = db.Column(db.String(255))
    medium_display = db.Column(db.Text)
    exhibition_history = db.Column(db.Text)
    is_on_view = db.Column(db.Boolean)
    artwork_type_title = db.Column(db.String(255))
    department_title = db.Column(db.String(255))

    artist_id = db.Column(db.Integer, db.ForeignKey('artist.id'), nullable = True) #
    artist = db.relationship("Artist", back_populates='artworks')

    artist_title = db.Column(db.String(255))
    style_title = db.Column(db.String(255))
    image_id = db.Column(db.String(36))  # Assuming UUID is stored as string
    source_updated_at = db.Column(db.DateTime)

    exhibitions = db.relationship('Exhibition', secondary=exhibitions_artworks_table, back_populates='artworks') #lazy = true
    exhibition_ids = db.Column(db.ARRAY(db.Integer)) # Most likely not necessary, as we use the exhibition ones to link already.
    


    # ------------
    # generate image urls
    # ------------
    def generate_iiif_url(self, alt_image_ids=None):
        base_url = "https://www.artic.edu/iiif/2"
        image_ids = [self.image_id]
        if alt_image_ids:
            image_ids.extend(alt_image_ids)
        iiif_urls = [f"{base_url}/{image_id}/full/843,/0/default.jpg" for image_id in image_ids]
        return iiif_urls

    def searchable_text(self):
        return f"{self.title} {self.medium_display} {self.artwork_type_title} {self.artist_title}"

    # ------------
    # serialize
    # ------------
    def serialize(self):
        """
        returns a dictionary
        """
        return {
            'id': self.id,
            'api_model': self.api_model,
            'api_link': self.api_link,
            'title': self.title,
            'alt_titles': self.alt_titles,
            'date_start': self.date_start,
            'date_end': self.date_end,
            'date_display': self.date_display,
            'artist_display': self.artist_display,
            'place_of_origin': self.place_of_origin,
            'medium_display': self.medium_display,
            'exhibition_history': self.exhibition_history,
            'is_on_view': self.is_on_view,
            'artwork_type_title': self.artwork_type_title,
            'department_title': self.department_title,
            'artist_id': self.artist_id,
            'artist_title': self.artist_title,
            'style_title': self.style_title,
            'image_id': self.image_id,
            'source_updated_at': self.source_updated_at.isoformat() if self.source_updated_at else None,
            }


class Artist(db.Model):
    __tablename__ = 'artist'

    id = db.Column(db.Integer, primary_key=True)
    api_model = db.Column(db.String(255))
    api_link = db.Column(db.String(255))
    title = db.Column(db.String(255))
    sort_title = db.Column(db.String(255))
    alt_titles = db.Column(db.ARRAY(db.String(255)))
    birth_date = db.Column(db.Integer)
    death_date = db.Column(db.Integer)
    description = db.Column(db.Text)
    ulan_id = db.Column(db.Integer)
    source_updated_at = db.Column(db.DateTime)
    # is_artist = db.Column(db.Boolean, default=True)

    artworks = db.relationship('Artwork', back_populates='artist') # lazy = true
    exhibitions = db.relationship('Exhibition', secondary=exhibitions_artists_table, back_populates='artists') #lazy = true

    def searchable_text(self):
        srch_txt = f"{self.title}"
        if self.description:
            srch_txt = srch_txt + f" {self.description}"
        if self.birth_date:
            srch_txt = srch_txt + f" {self.birth_date}"
        if self.death_date:
            srch_txt = srch_txt + f" {self.death_date}"
        if self.alt_titles:
            for alt_title in self.alt_titles:
                srch_txt = srch_txt + f" {alt_title}"

        # print(srch_txt + "\n") # DEBUG

        return srch_txt
    
    # ------------
    # serialize
    # ------------
    def serialize(self):
        """
        returns a dictionary
        """
        return {
            'id': self.id,
            'api_model': self.api_model,
            'api_link': self.api_link,
            'title': self.title,
            'sort_title': self.sort_title,
            'alt_titles': self.alt_titles,
            'birth_date': self.birth_date,
            'death_date': self.death_date,
            'description': self.description,
            'ulan_id': self.ulan_id,
            'source_updated_at': self.source_updated_at.isoformat() if self.source_updated_at else None,
        }

# Define a reusable function to attach the DDL statements
def add_tsvector_column_and_index(table_name):
    tsvector_ddl = DDL(f"""
        ALTER TABLE {table_name} ADD COLUMN tsv tsvector GENERATED ALWAYS AS (to_tsvector('english', title)) STORED;
        CREATE INDEX ix_{table_name}_tsv ON {table_name} USING gin(tsv);
    """)
    event.listen(
        eval(table_name.capitalize()).__table__,
        'after_create',
        tsvector_ddl.execute_if(dialect='postgresql')
    )

with app.app_context():
    db.create_all()

    # Attach the DDL statements to each model
    add_tsvector_column_and_index('exhibition')
    add_tsvector_column_and_index('artist')
    add_tsvector_column_and_index('artwork')


